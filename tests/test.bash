#!/bin/bash

# Copyright © 2016-2018 Dmytro Katyukha <dmytro.katyukha@gmail.com>

#######################################################################
# This Source Code Form is subject to the terms of the Mozilla Public #
# License, v. 2.0. If a copy of the MPL was not distributed with this #
# file, You can obtain one at http://mozilla.org/MPL/2.0/.            #
#######################################################################

# this script run's basic tests

SCRIPT=$0;
SCRIPT_NAME=$(basename $SCRIPT);
PROJECT_DIR=$(readlink -f "$(dirname $SCRIPT)/..");
TEST_TMP_DIR="${TEST_TMP_DIR:-$PROJECT_DIR/test-temp}";
WORK_DIR=$(pwd);

ERROR=;

tempfiles=( )

# do cleanup on exit
cleanup() {
  if [ -z $ERROR ]; then
      if ! rm -rf "$TEST_TMP_DIR"; then
          echo "Cannot remove $TEST_TMP_DIR";
      fi
  fi
}
trap cleanup 0

# Handle errors
# Based on: http://stackoverflow.com/questions/64786/error-handling-in-bash#answer-185900
error() {
  local parent_lineno="$1"
  local message="$2"
  local code="${3:-1}"
  ERROR=1;
  if [[ -n "$message" ]] ; then
    echo "Error on or near line ${parent_lineno}: ${message}; exiting with status ${code}"
  else
    echo "Error on or near line ${parent_lineno}; exiting with status ${code}"
  fi
  exit "${code}"
}
trap 'error ${LINENO}' ERR

# Fail on any error
set -e;

# Init test tmp dir
mkdir -p $TEST_TMP_DIR;
cd $TEST_TMP_DIR;

# Prepare for test (if running on CI)
source "$PROJECT_DIR/tests/ci.bash";

# import odoo-helper common lib to allow colors in test output
source $(odoo-helper system lib-path common);
allow_colors;

#
# Start test
# ==========
#
echo -e "${YELLOWC}
===================================================
Show odoo-helper-scripts version
===================================================
${NC}"
odoo-helper --version;


echo -e "${YELLOWC}
===================================================
Install odoo-helper and odoo system prerequirements
===================================================
${NC}"

odoo-helper install pre-requirements -y;
odoo-helper install bin-tools -y;
odoo-helper install postgres;

if [ ! -z $CI_RUN ] && ! odoo-helper exec postgres_test_connection; then
    echo -e "${YELLOWC}WARNING${NC}: Cannot connect to postgres instance. Seems that postgres not started, trying to start it now..."
    sudo /etc/init.d/postgresql start;
fi


echo -e "${YELLOWC}
===================================================
Run odoo-helper postgres speedify
===================================================
${NC}"
odoo-helper postgres speedify

# Odoo 14 runs only with python 3.6+
echo -e "${YELLOWC}
=================================
Install and check Odoo 14.0 (Py3)
=================================
${NC}"

cd ../;
odoo-helper install sys-deps -y 14.0;


if python3 -c "import sys; exit(sys.version_info < (3, 6));"; then 
    # Odoo 14 runs only with python 3.6+
    odoo-install --install-dir odoo-14.0 --odoo-version 14.0 \
        --http-port 8469 --http-host local-odoo-14 \
        --db-user odoo14 --db-pass odoo --create-db-user
else
    # System python is less then 3.6, so build python 3.7 to use for
    # this odoo version
    odoo-install --install-dir odoo-14.0 --odoo-version 14.0 \
        --http-port 8469 --http-host local-odoo-14 \
        --db-user odoo14 --db-pass odoo --create-db-user \
        --build-python 3.7.0
fi

cd odoo-14.0;

# Install py-tools and js-tools
odoo-helper install py-tools;
odoo-helper install js-tools;

odoo-helper server run --stop-after-init;  # test that it runs

# Show project status
odoo-helper status;
odoo-helper server status;
odoo-helper start;
odoo-helper ps;
odoo-helper status;
odoo-helper server status;
odoo-helper stop;

# Show complete odoo-helper status
odoo-helper status  --tools-versions --ci-tools-versions;

# Database management
odoo-helper db create --demo --lang en_US odoo14-odoo-test;

# Fetch oca/contract
odoo-helper fetch --github crnd-inc/generic-addons

# Install addons from OCA contract
odoo-helper addons install --ual --dir ./repositories/crnd-inc/generic-addons;

# Fetch bureaucrat_helpdesk_lite from Odoo market and try to install it
odoo-helper fetch --odoo-app bureaucrat_helpdesk_lite;
odoo-helper addons install --ual bureaucrat_helpdesk_lite;

# Print list of installed addons
odoo-helper addons find-installed;

# Drop created databases
odoo-helper db drop odoo14-odoo-test;

echo -e "${YELLOWC}
=================================
Install and check Odoo 15.0 (Py3)
=================================
${NC}"

cd ../;
odoo-helper install sys-deps -y 15.0;


if python3 -c "import sys; exit(sys.version_info < (3, 6));"; then 
    # Odoo 14 runs only with python 3.6+
    odoo-install --install-dir odoo-15.0 --odoo-version 15.0 \
        --http-port 8569 --http-host local-odoo-15 \
        --db-user odoo15 --db-pass odoo --create-db-user
else
    # System python is less then 3.6, so build python 3.7 to use for
    # this odoo version
    odoo-install --install-dir odoo-15.0 --odoo-version 15.0 \
        --http-port 8569 --http-host local-odoo-15 \
        --db-user odoo15 --db-pass odoo --create-db-user \
        --build-python 3.7.0
fi

cd odoo-15.0;

# Install py-tools and js-tools
odoo-helper install py-tools;
odoo-helper install js-tools;

odoo-helper server run --stop-after-init;  # test that it runs

# Show project status
odoo-helper status;
odoo-helper server status;
odoo-helper start;
odoo-helper ps;
odoo-helper status;
odoo-helper server status;
odoo-helper stop;

# Show complete odoo-helper status
odoo-helper status  --tools-versions --ci-tools-versions;

# Database management
odoo-helper db create --demo --lang en_US odoo15-odoo-test;

sleep 5s

# Fetch oca/contract
odoo-helper fetch --github crnd-inc/generic-addons

# Install addons from OCA contract
odoo-helper addons install --ual --dir ./repositories/crnd-inc/generic-addons;

# Fetch bureaucrat_helpdesk_lite from Odoo market and try to install it
odoo-helper fetch --odoo-app bureaucrat_helpdesk_lite;
odoo-helper addons install --ual bureaucrat_helpdesk_lite;

# Print list of installed addons
odoo-helper addons find-installed;

# Drop created databases
odoo-helper db drop odoo15-odoo-test;

echo -e "${YELLOWC}
=============================================================
Run 'prepare-docs' script to test generation of help messages
=============================================================
${NC}"

bash "$PROJECT_DIR/scripts/prepare_docs.bash";

echo -e "${GREENC}
==========================================
Tests finished
==========================================
${NC}"

